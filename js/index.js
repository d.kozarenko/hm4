"use strict";

const BASE_URL = "https://ajax.test-danit.com/api/swapi/";
const ENTITIES = ["films", "people"];

class Characters {
  constructor(array) {
    this.array = array;
  }
  request(url) {
    return fetch(url);
  }
  getInfo() {
    const arr = [];
    this.array.forEach((elem, value) => {
      this.request(elem)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          arr.push(data.name);
        });
    });
    return arr;
  }
}

class Film {
  constructor(url) {
    this.url = url;
  }
  request() {
    return fetch(this.url + ENTITIES[0]);
  }
  getInfo() {
    this.request()
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        data.forEach((film) => {
          const characters = new Characters(film.characters);
          this.createList().append(
            this.renderName(film.name),
            this.renderId(film.id),
            this.renderCrawl(film.openingCrawl)
          );
          this.renderCharacters(characters.getInfo());
        });
      });
  }

  createList() {
    const ul = document.createElement("ul");
    document.querySelector(".root").append(ul);
    return ul;
  }
  renderCharacters(data) {
    const list = this.createList();
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(data);
      }, 5000);
    }).then((newData) => {
      const allData = newData.join(", ");
      const ul = document.createElement("ul");
      const li = document.createElement("li");
      li.append(allData);
      ul.append(li);
      list.append(ul);
    });
  }
  renderName(name) {
    const li = document.createElement("li");
    const p = document.createElement("p");
    p.textContent = name;
    p.style.fontWeight = "700";
    li.append(p);
    return li;
  }
  renderId(id) {
    const ul = document.createElement("ul");
    const li = document.createElement("li");
    const p = document.createElement("p");
    p.textContent = `Episode: ${id}`;
    li.append(p);
    ul.append(li);
    return ul;
  }
  renderCrawl(crawl) {
    const ul = document.createElement("ul");
    const li = document.createElement("li");
    const p = document.createElement("p");
    p.textContent = crawl;
    li.append(p);
    ul.append(li);
    return ul;
  }
}

const film = new Film(BASE_URL);
film.getInfo();
